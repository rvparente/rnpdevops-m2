variable "project_id" {
    description = "Identificador do Projeto n GCP"
}
variable "secret" {
    description = "Chave de acesso ao GCP"
}
variable "image_name" {
    description = "Nome da imagem para utilizar"
}

provider "google" {
    credentials = var.secret
	project = "cursodevops-292221"
	region = "us-central1"
}

resource "google_compute_instance" "default" {
  name         = var.image_name
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"

  boot_disk {
    initialize_params {
      image = var.image_name
    }
  }
  network_interface {
    network = "default"
  }
}